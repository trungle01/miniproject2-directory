package edu.sjsu.android.miniproject2;

import android.os.Parcel;
import android.os.Parcelable;

public class Group implements Parcelable {
    private int imageId;
    private String name;
    private String detail;

    public Group(int imageId, String name, String detail){
        this.imageId = imageId;
        this.name = name;
        this.detail = detail;
    }

    protected Group(Parcel in) {
        imageId = in.readInt();
        name = in.readString();
        detail = in.readString();
    }

    public static final Creator<Group> CREATOR = new Creator<Group>() {
        @Override
        public Group createFromParcel(Parcel in) {
            return new Group(in);
        }

        @Override
        public Group[] newArray(int size) {
            return new Group[size];
        }
    };

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(imageId);
        parcel.writeString(name);
        parcel.writeString(detail);
    }
}
