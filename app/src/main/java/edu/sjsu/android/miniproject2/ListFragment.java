package edu.sjsu.android.miniproject2;

import android.app.AlertDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * A fragment representing a list of Items.
 */
public class ListFragment extends Fragment implements MyAdapter.OnListListener {

    private ArrayList<Group> data;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        generateData();
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        RecyclerView recyclerView = (RecyclerView) view;
        recyclerView.setAdapter(new MyAdapter(data, this));
        return view;
    }

    private void generateData() {
        data = new ArrayList<>();
        data.add(new Group(R.drawable.arashi, "Arashi", "Arashi detail: Arashi is a Japanese boy band consisting of five members formed under the Johnny & Associates talent agency. The members are Satoshi Ohno, Sho Sakurai, Masaki Aiba, Kazunari Ninomiya, and Jun Matsumoto. Arashi officially formed on September 15, 1999, in Honolulu, Hawaii, and made their debut CD on November 3, 1999. The group was initially signed to Pony Canyon and released one studio album and six singles—beginning with their 1999 eponymous debut single before moving to the Johnny's subsidiary label J Storm in 2001, which was initially set up for their succeeding releases.[2] While their debut single sold close to a million copies, the group took a mixture of pop and alternate/contemporary approach to its music which attracted core followers, but subsequently faced commercially slow sales."));
        data.add(new Group(R.drawable.kattun, "Kattun", "Kattun detail: Kattun is a Japanese boy band formed under Johnny & Associates (Johnny's) in 2001. The group's name was originally an acronym based on the first letter of each member's family name: Kazuya Kamenashi, Jin Akanishi, Junnosuke Taguchi, Koki Tanaka, Tatsuya Ueda, and Yuichi Nakamaru. Their debut on March 22, 2006 was marked by a tripartite release of a CD single, album and music DVD on their exclusive record label J-One Records. Since then, all of their single, album and music DVD releases have debuted at number one on the Oricon music and DVD charts."));
        data.add(new Group(R.drawable.big_bang, "BigBang", "BigBang detail: Bigbang is a South Korean boy band formed by YG Entertainment. The group consists of four members: G-Dragon, T.O.P, Taeyang, and Daesung; former member Seungri retired from the entertainment industry on March 11, 2019.[2] Dubbed the \"Kings of K-pop\", they helped spread the Korean Wave internationally and are one of the most influential acts in K-pop history.[3][4][5][6][7][8] They are known for their trendsetting musical experimentation, self-production, and stage presence."));
        data.add(new Group(R.drawable.blackpink, "BlackPink", "BlackPink detail: BlackPink is a South Korean girl group formed by YG Entertainment, consisting of members Jisoo, Jennie, Rosé and Lisa. The group debuted in August 2016 with their single album Square One, which featured \"Whistle\" and \"Boombayah\", their first number-one entries on South Korea's Gaon Digital Chart and the Billboard World Digital Song Sales chart, respectively."));
        data.add(new Group(R.drawable.puppy, "Puppy", "Puppy detail: Puppy content"));
    }

    @Override
    public void onListClick(int position, List<Group> list) {
        Group item = data.get(position); // get each row data
        View view = getView();
        if(position == list.size() - 1) {
            // add a warning if the last item is clicked
            warn(position, view);
        }
        else {
            // send data (group, view) from ListFragment to DetailFragment
            goToDetail(position, view);
        }
    }

    private void goToDetail(int position, View view) {
        Bundle bundle = new Bundle();
        Group item = data.get(position);
        bundle.putParcelable("info", item);
        ListFragment fragment = new ListFragment();
        fragment.setArguments(bundle);
        NavController controller = Navigation.findNavController(view);
        controller.navigate(R.id.action_listFragment_to_detailFragment, bundle);
    }

    // Warn(): Warning when the last item is clicked
    private void warn(int position, View view){
        AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
        builder.setTitle("Warning!!!");
        builder.setMessage("Last item is clicked!!!");
        builder.setPositiveButton("Yes", (dialogInterface, i) -> {
            // when user select yes
            goToDetail(position, view);
        });
        builder.setNegativeButton("No", (dialogInterface, i) -> {
            // when user select No => do nothing
        });
        builder.create().show();
    }
}