package edu.sjsu.android.miniproject2;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextClock;

import edu.sjsu.android.miniproject2.databinding.FragmentInfoBinding;

public class InfoFragment extends Fragment{
    private FragmentInfoBinding binding;

    public InfoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = FragmentInfoBinding.inflate(getLayoutInflater());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentInfoBinding binding = FragmentInfoBinding.inflate(getLayoutInflater());
        binding.btnCall.setOnClickListener(this::dialPhone);
        return binding.getRoot();
    }

    public void dialPhone(View view){
        // figure out how to dial call
        startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse("tel:+14086794123")));
    }

}