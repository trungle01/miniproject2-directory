package edu.sjsu.android.miniproject2;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import edu.sjsu.android.miniproject2.databinding.RowLayoutBinding;
import edu.sjsu.android.miniproject2.placeholder.PlaceholderContent.PlaceholderItem;


import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link PlaceholderItem}.
 */
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

    private final List<Group> mValues;
    private final OnListListener monListListener;

    public MyAdapter(List<Group> items, OnListListener onListListener) {
        mValues = items;
        this.monListListener = onListListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(RowLayoutBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false), monListListener);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        // group is a list. get value from mValues
        Group group = mValues.get(position);
        holder.binding.rowImage.setImageResource(group.getImageId());
        holder.binding.content.setText(group.getName());
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final RowLayoutBinding binding;
        OnListListener onListListener;

        public ViewHolder(RowLayoutBinding binding, OnListListener onListListener) {
            super(binding.getRoot());
            this.binding = binding;
            this.onListListener = onListListener;

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            onListListener.onListClick(getAdapterPosition(), mValues);
        }
    }


    public interface OnListListener{
        void onListClick(int position, List<Group> list);
    }
}