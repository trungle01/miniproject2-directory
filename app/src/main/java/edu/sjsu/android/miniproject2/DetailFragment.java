package edu.sjsu.android.miniproject2;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import edu.sjsu.android.miniproject2.databinding.FragmentDetailBinding;


public class DetailFragment extends Fragment {
    private Group receivedGroup;

    public DetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentDetailBinding binding = FragmentDetailBinding.inflate(getLayoutInflater());
        Bundle bundle = getArguments();
        if(bundle != null){
            receivedGroup = bundle.getParcelable("info");
            binding.textViewDetail.setText(receivedGroup.getName());
            binding.imageViewDetail.setImageResource(receivedGroup.getImageId());
            binding.textViewContent.setText(receivedGroup.getDetail());
        }

        return binding.getRoot();
    }
}